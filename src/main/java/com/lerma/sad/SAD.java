package com.lerma.sad;

import java.util.*;

//Singleton enum, this directory will control everything.

public enum SAD implements ListingTypes, Sorts {

	INSTANCE;
	
	private CategoryTree categories = new CategoryTree("root"); //contains listings and the categories they are associated with
	private Map<Integer, AdvertiserListings> advertisers = new HashMap<>(); //for O(1) lookup on advertisers
	private Map<Integer, Listing> listings = new HashMap<>(); //for O(1) lookup on listings
	
	private SAD() { }
	
	public CategoryTree getCategories() {
		return categories;
	}


	public Map<Integer, AdvertiserListings> getAdvertiserMap() {
		return advertisers;
	}

	public void addAdvertiser(Advertiser adv) {
		AdvertiserListings al = new AdvertiserListings(adv);
		advertisers.put(al.getAdvertiser().getID(), al);
	}

	public boolean addListingToAdvertiser(Listing l) {
		if (advertisers.containsKey(l.getAdvertiserID())) {
			AdvertiserListings al = advertisers.get(l.getAdvertiserID());
			al.addListing(l);
			advertisers.replace(l.getAdvertiserID(), al);
			return true;
		}
		return false;
	}

	private void addListingToListingMap(Listing l) {
		listings.put(l.getID(), l);
	}

	private void removeListingFromListingMap(int id) {
		listings.remove(id);
	}
	
	public boolean addListing(Listing l) {
		if(!addListingToAdvertiser(l))
			return false;
		if(!categories.insertListingToCategory(l))
			return false;
		addListingToListingMap(l);
		return true;
	}
	
	public Listing getListingByID(int id) {
		return listings.get(id);
	}


	
	public Map<String, List<Listing>> getListingsForACategory(List<String> cats) {
		return categories.getListingsForACategory(cats);
	}


	public List<Searchable> queryResults(String query) {
		List<Searchable> items = new ArrayList<>();
		for(int i : advertisers.keySet()) {
			AdvertiserListings a = advertisers.get(i);
			if (a.getAdvertiser().matches(query))
				items.add(a.getAdvertiser());
			for(Listing l : a.getListings()) {
				if (l.matches(query))
					items.add(l);
			}
		}
		return items;
	}

	public List<Listing> getListingsByType(int type) {
        List<Listing> typelist = new ArrayList<>();
        for (int id : listings.keySet()) {
            if (listings.get(id).getType() == type)
                typelist.add(listings.get(id));
        }
        return typelist;
    }


	public List<AdvertiserListings> getAllAdvertiserListings() {
		List<AdvertiserListings> al = new ArrayList<AdvertiserListings>();
		advertisers.forEach((k, v) -> al.add(v));
		return al;
	}

	public List<Advertiser> getAllAdvertisers() {
		List<Advertiser> ads = new ArrayList<Advertiser>();
		advertisers.forEach((k, v) -> ads.add(v.getAdvertiser()));
		return ads;
	}

	public List<Listing> getListings() {
		List<Listing> copy = new ArrayList<>();
		for(int id: listings.keySet()) {
			copy.add(listings.get(id));
		}
		return copy;
	}

    public List<Searchable> queryResult(String query) {
        List<Searchable> queryResults = new ArrayList<>();
        for(int id : advertisers.keySet()) {
            if(advertisers.get(id).matches(query))
                queryResults.add(advertisers.get(id));
        }

        queryResults.addAll(categories.matches(query));
        return queryResults;
    }

	public AdvertiserListings getAdvertiser(int id) {
		return advertisers.get(id);
	}

	public void sortedByAdvertiserName(List<Listing> listings) {
		Collections.sort(listings, (l1, l2) -> l1.getAdvName().compareTo(l2.getAdvName()));
	}
	
	public void sortByMostRecent(List<Listing> listings) {
		Collections.sort(listings, (l1, l2) -> l1.compareToByMostRecent(l2));
	}
	
	public void sortByMostProfittable(List<Listing> listings) {
		Collections.sort(listings, (l1, l2) -> l1.compareToByMostProfittable(l2));
	}
	
	public void sortByRandom(List<Listing> listings) {
		Collections.shuffle(listings);
	}
	

}
