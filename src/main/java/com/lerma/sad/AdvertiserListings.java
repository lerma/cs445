package com.lerma.sad;

import java.util.ArrayList;
import java.util.List;


public class AdvertiserListings implements ListingTypes, Searchable {

	private Advertiser advertiser;
	private List<Listing> listings;
	private List<Listing> deleted;
	
	public AdvertiserListings() { }
	
	public AdvertiserListings(Advertiser advertiser) {
		this.advertiser = advertiser;
		listings = new ArrayList<>();
		deleted = new ArrayList<>();
	}

	public Advertiser getAdvertiser() {
		return advertiser;
	}

	public void setAdvertiser(Advertiser advertiser) {
		this.advertiser = advertiser;
	}
	
	public List<Listing> getListings() {
		return listings;
	}

	public void setListings(List<Listing> listings) {
		this.listings = listings;
	}
	
	public List<Listing> getDeleted() {
		return deleted;
	}

	public void setDeleted(List<Listing> deleted) {
		this.deleted = deleted;
	}


	public boolean addListing(Listing l) {
		if(l == null)
			return false;
		listings.add(l);
		return true;
	}

	public boolean deleteListing(int id) {
		for(Listing l : listings) {
			if(l.getID() == id) {
				listings.remove(l);
				return true;
			}
		}
		return false;
	}
	
	public List<Listing> getListingsByType(int type) {
		if(type < 1 || type > 4)
			return null;
		
		List<Listing> list = new ArrayList<Listing>();
		for(Listing l : listings) {
			if(l.getType() == type)
				list.add(l);
		}
		return list;
	}

	public boolean matches(String query) {
		if(advertiser.matches(query))
			return true;

		for(Listing l : listings) {
			if(l.matches(query))
				return true;
		}

		for(Listing l : deleted) {
			if(l.matches(query))
				return true;
		}

		return false;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Advertiser:\n" + advertiser.toString() + "\n");
		sb.append("Listings: \n");
		for(Listing l : listings) {
			sb.append(l.toString());
		}
		return sb.toString();
	}

}
