package com.lerma.sad;

import java.util.ArrayList;
import java.util.List;

public class Category implements ListingTypes, Searchable {

	private List<Listing> listings;
	private List<Category> children;
	private Category parent;
	private String name; 
	private String parentName;
	
	public Category() { }
	
	public Category(String name) {
		children = new ArrayList<>();
		listings = new ArrayList<>();
		parent = null;
		parentName = null;
		this.name = name.toLowerCase();
	}
	
	public Category(String name, String parentName) {
		this(name);
		this.parentName = parentName;
	}

	public Category getParent() {
		return this.parent;
	}
	
	public void setParent(Category parent) {
		this.parent = parent;
		setParentName(parent.getName());

	}
	
	public String getParentName() {
		return parentName;
	}
	
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	
	public List<Category> getChildren() {
		return this.children;
	}
	
	public void setChildren(List<Category> children) {
		children.forEach(child -> {
			child.parent = this; 
			child.parentName = name;
		});
		this.children = children;
	}
	
	public void setName(String name) {
		this.name = name.toLowerCase();
	}
	
	public String getName() {
		return name;
	}
	
	
	public int getNumberOfChildren() {
		return getChildren().size();
	}
	
	public boolean hasChildren() {
		return getNumberOfChildren() > 0;
	}
	
	
	public void addChild(Category child) {
		if(child.parentName.equals(name)) {
			child.parent = this;
			child.parentName = name;
			children.add(child);
		} else {
			for(Category kid: children) {
				kid.addChild(child);
			}
		}
	}

	
	public void addChildAt(int index, Category child) throws IndexOutOfBoundsException {
		child.parent = this;
		children.add(index, child);
	}
	
	
	public void removeChildren() {
		this.children = new ArrayList<>();
	}
	
	public void removeChildAt(int index) throws IndexOutOfBoundsException {
		children.remove(index);
	}

	public void removeChildByName(String name) {
		for (Category child: children) {
			if (child.getName().equalsIgnoreCase(name)) {
				children.remove(child);
				return;
			}
		}
	}


	public Category getChildAt(int index) throws IndexOutOfBoundsException {
		return children.get(index);
	}
	
	public Category getChildWithName(String name) {
		for(Category child : children) {
			if(child.name.equals(name))
				return child;
		}
		return null;
	}
	
	public Category getRoot() {

		Category temp = this;
		while(!temp.isRoot()) {
			temp = temp.parent;
		}
		return temp;
	}
	
	public boolean addListing(Listing l) {
		if (l == null)
			return false;
		listings.add(l);
		return true;
	}

	
	public boolean removeListing(int id) {
		for(Listing l : listings) {
			if(l.getID() == id) {
				listings.remove(l);
				return true;
			}
		}
		return false;
	}

	public List<Listing> getListings() {
		return listings;
	}
	
	public List<Listing> getFeaturedListings() {
		List<Listing> featured = new ArrayList<Listing>();
		for(Listing l : listings) {
			if(l.getType() == FEATURED)
				featured.add(l);
		}
		return featured;
	}

	public List<Listing> getNormalListings() {
		List<Listing> normal = new ArrayList<Listing>();
		for(Listing l : listings) {
			if(l.getType() == NORMAL)
				normal.add(l);
		}
		return normal;
	}
	
	public boolean isRoot() {
		return parent == null;
	}
	
	public boolean equals(Object obj) {
		
		Category cat  = (Category)obj;
		if(cat.getName().equals(name))
			return true;

		return false;
	}

	
	public boolean hasListing(Listing l) {
		for(Listing listing : listings) {
			if(listing.getID() == l.getID())
				return true;
		}
		return false;
	}

	
	public String listToString() {
		StringBuilder featuredStr = new StringBuilder();
		featuredStr.append("FEATURED: \n");
		StringBuilder normalStr = new StringBuilder();
		normalStr.append("NORMAL: \n");
		
		List<Listing> featured = new ArrayList<Listing>();
		List<Listing> normal = new ArrayList<Listing>();
		
		for(Listing l : listings) {
			if(l.getType() == FEATURED) {
				featured.add(l);
				featuredStr.append(l + "\n");
			}
			else if (l.getType() == NORMAL) {
				normal.add(l);
				normalStr.append(l + "\n");
			}
		}
		featuredStr.append(normalStr);
		
		return featuredStr.toString();
	}

	public boolean matches(String query) {
		return query.contains(name);
	}
	
	public String toString() {
		return name;
	}
}
