package com.lerma.sad;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;


@XmlRootElement
public class Advertiser implements Comparable<Advertiser>, Searchable {
	
	private static int IDCOUNT = 0; //class counter for when user does not specify
	private int id;
	private String name, email, phone, facebook, twitter, linkedin;
	private Address address; 
	
	public Advertiser() {  }
	
	public Advertiser(String name, String email, String phone, Address address) {
		this.name = name;
		this.email = email;
		this.address = address;
	}
	
	public Advertiser(int id, String name, String email, String phone, Address address) {
		this(name, email, phone, address);
		this.id = id;
		this.facebook = "";
		this.twitter = "";
		this.linkedin = "";
	}
	
	public Advertiser(int id, String name, String email, String phone, 
			Address address, String facebook, String twitter, String linkedin) {
		this(id, name, email, phone, address);
		this.facebook = facebook;
		this.twitter = twitter;
		this.linkedin = linkedin;
	}
	
	public static boolean validate(Advertiser adv) {
		String[] arr = {adv.facebook, adv.twitter, adv.linkedin};
		for(String social : arr) {
			if(social != null) {
				if(!Validators.isValidURL(social))
					return false;
			}
		}
		return Validators.isValidEmail(adv.getEmail()) && Validators.isValidPhoneNumber(adv.getPhone());
	}
	
	public static int getCurrentIDCount() {
		return IDCOUNT;
	}
	
	public static int getThenIncrementIDCount() {
		return IDCOUNT++;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public int getID() {
		return id;
	}
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String facebook) {
		if(Validators.isValidEmail(facebook))
			this.facebook = facebook;
		else 
			throw new IllegalArgumentException();
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		if(Validators.isValidPhoneNumber(phone))
			this.phone = phone;
		else
			throw new IllegalArgumentException();
	}

	public String getFacebook() {
		return facebook;
	}

	public void setFacebook(String facebook) {
		if(Validators.isValidURL(facebook))
			this.facebook = facebook;
		else 
			throw new IllegalArgumentException();
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		if(Validators.isValidEmail(twitter))
			this.twitter = twitter;
		else 
			throw new IllegalArgumentException();
	}

	public String getLinkedin() {
		return linkedin;
	}

	public void setLinkedin(String linkedin) {
		if(Validators.isValidEmail(linkedin))
			this.linkedin = linkedin;
		else 
			throw new IllegalArgumentException();
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public int compareTo(Advertiser adv) {
		int compare = name.compareTo(adv.getName());
		if(compare < 0)
			return -1;
		else if (compare > 0 )
			return 1;
		else 
			return 0;
	}
	
	public boolean matches(String str) {
		if(str.equals(phone)) 
			return true;
		
		String[] arr = { name, email, facebook, twitter, linkedin };
		for(String s : arr) {
			if( s == null)
				continue;
			if ( s.toLowerCase().contains(str.toLowerCase()) )
				return true;
		}
		
		return false;
	}
	
	public boolean equals(Advertiser adv) {
		return id == adv.getID();
	}
	
	public Map<String, Integer> getIDAsHashMap() {
		Map<String, Integer> map = new HashMap<>(1);
		map.put("id", id);
		return map;
	}
	
	
	public String toString() {
		String str = "Advertiser Name: " + name + " \n";
		str += "Email Address: " + email + " Phone Number: " + phone + "\n";
		str += "Social Media \n";
		if(facebook != null)
			str += "Facebook: " + facebook + "\n";
		if(twitter != null)
			str += "Twitter: " + twitter + "\n";
		if(linkedin != null)
			str += "LinkedIn: " + linkedin + "\n";
		str += "ID: " + id +  "\n";
		return str;
	}
}
