package com.lerma.sad;

import com.google.gson.Gson;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;
import java.util.List;

@Path("/admin")
public class AdminResource {
	
	@Context
	private UriInfo uriInfo;
	@Context
	private Request request;
	
	private Gson gson = new Gson();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/advertiser")
	public Response postAdvertiser(Advertiser adv) {
		if(Advertiser.validate(adv)) {
			adv.setID(Advertiser.getThenIncrementIDCount());
			SAD.INSTANCE.addAdvertiser(adv);
			return Response.status(Status.CREATED).entity(adv.getIDAsHashMap()).build();
		} 
		return Response.status(Status.BAD_REQUEST).build();
	
	}
	
	//get a list of all advertisers
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/advertiser")
	public Response getAllAdvertisers() {
		List<Advertiser> ad = SAD.INSTANCE.getAllAdvertisers();
		return Response.ok(ad).build();
	}

	//put a new advertiser with specific id
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/advertiser/{aid}")
	public Response putAdvertiser(@PathParam("aid") int id, Advertiser advertiser) {
		
		if(Advertiser.validate(advertiser)) {
			advertiser.setID(id);
			SAD.INSTANCE.addAdvertiser(advertiser);
			return Response.ok(advertiser.getIDAsHashMap()).status(Status.CREATED).build();
		}

		return Response.status(Status.BAD_REQUEST).build();

	}
	
	//get an advertiser with a specific id
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/advertiser/{aid}")
	public Response getAdvertiserByID(@PathParam("aid") int id) {

		AdvertiserListings adv = SAD.INSTANCE.getAdvertiser(id);
		if(adv == null)
			return Response.status(Status.BAD_REQUEST).entity("Not Found").build();

		return Response.ok(adv).build();
	}
	
	//create a new listing, id is assigned by static count
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listing")
	public Response postListing(Listing listing) {
		if(!Listing.validListing(listing))
			return Response.status(Status.BAD_REQUEST).entity("Illegal Format").build();
		listing.setId(Listing.getThenIncrementCount());
		if(!SAD.INSTANCE.addListing(listing))
			return Response.status(Status.BAD_REQUEST).entity("Illegal Format").build();
		return Response.ok(listing.getIDAsHashMap()).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listing")
	public Response getListing() {
		return Response.status(Status.ACCEPTED).entity(SAD.INSTANCE.getListings()).build();

	}
	
	//create a new listing with a specific id
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listing/{lid}")
	public Response putListing(@PathParam("lid") int id, Listing listing) {
		
		if(!Listing.validListing(listing))
			return Response.status(Status.BAD_REQUEST).entity("Illegal Format").build();
		
		listing.setId(id);
		
		if(!SAD.INSTANCE.addListing(listing))
			return Response.status(Status.BAD_REQUEST).entity("Illegal Format").build();
		
		return Response.ok("{ id: " + listing.getID() + " }").build();
	}

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/listing/{lid}")
    public Response getListingByID(@PathParam("lid") int id) {
        Listing found = SAD.INSTANCE.getListingByID(id);
        if(found == null) {
            return Response.status(Status.BAD_REQUEST).entity("Nothing found").build();
        }
        return Response.status(Status.OK).entity(found).build();
    }

	
	
	//creates a new category with no parent to the root
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/category")
	public Response postCategory(CategoryWrapper cat) {

		if(cat.getName() == null)
			return Response.status(Status.BAD_REQUEST).build();

		SAD.INSTANCE.getCategories().addCategory("root", cat.getName());
		return Response.status(Status.CREATED).entity(SAD.INSTANCE.getCategories().findCategoryName(cat.getName()).getName()).build();
	}
	
	//all categories and listings are returned
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/category")
	public Response getCategories() {
		return Response.ok(SAD.INSTANCE.getCategories().toString()).build();
	}

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
    @Path("/category/{cid}")
	public Response getCategoryByID(@PathParam("cid") String cat) {
        Category c = SAD.INSTANCE.getCategories().findCategoryName(cat);
        if(c == null)
            return Response.status(Status.BAD_REQUEST).entity("Not Found").build();

        return Response.ok(c).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/category/{cid}")
    public Response putCategory(@PathParam("cid") String parent, CategoryWrapper cat) {


		if(!SAD.INSTANCE.getCategories().categoryExists(parent))
			return Response.status(Status.BAD_REQUEST).build();

		if(cat.getName() == null)
			return Response.status(Status.BAD_REQUEST).build();



        SAD.INSTANCE.getCategories().addCategory(parent, cat.getName());

        return Response.ok(SAD.INSTANCE.getCategories().findCategoryName(cat.getName())).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/category/{cid}")
    public Response deleteCategory(@PathParam("cid") String cat) {
        if(SAD.INSTANCE.getCategories().deleteCategory(cat)) {
            return Response.status(Status.OK).entity("Deleted Category: " + cat.toString()).build();
        }
        return Response.status(Status.BAD_REQUEST).entity("Not Found").build();
    }






}
