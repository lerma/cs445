package com.lerma.sad;

import java.util.*;

public class CategoryTree {

	
	private Category root;
	
	public CategoryTree() {}
	
	public CategoryTree(String rootname) {
		Category root = new Category(rootname);
		this.root = root;
	}
	
	public Category getRoot(){
		return root;
	}
	
	public void setRoot(Category root) {	
		this.root = root;
	}
	
	public int getNumberOfNodes() {
		int numberofNodes = 0;
		
		if(!isEmpty()) {
			numberofNodes = auxGetNumberOfNodes(root) + 1;
		}
		
		return numberofNodes;
	}
	
	private int auxGetNumberOfNodes(Category node) {
		int numberofnodes = node.getNumberOfChildren();
		
		for(Category child : node.getChildren()) {
			numberofnodes += auxGetNumberOfNodes(child);
		}
		
		return numberofnodes;
	}
	public boolean addCategory(String parentname, String newcat) {
		if(isEmpty()) {
			root = new Category("root");
			return false;
		}
		
		Category cat = new Category(newcat, parentname);
		root.addChild(cat);
		return true;
		
	}

	public boolean deleteCategory(String name) {
		if(isEmpty()) {
			return false;
		}

		return auxDeleteCategory(root, name);
	}

	private boolean auxDeleteCategory(Category cat, String name) {

		if(cat.getName().equals(name)) {
			for (Category c : cat.getChildren()) {
				c.setParent(cat.getParent());
				cat.getParent().addChild(c);
			}
			return true;
		} else {
			for(Category c : cat.getChildren()){
				return auxDeleteCategory(c, name);
			}
		}
		return false;
	}
	
	public boolean insertListingToCategory(Listing l) {
		Category found = findCategoryName(l.getCategory());
		if(found == null)
			return false;

		found.addListing(l);
		setRoot(found.getRoot());
		return true;
	}

    public Map<String, List<Listing>> getListingsForACategory(List<String> categories) {
        int i = 0;
        Category temp = findCategoryName(categories.get(i));
        if (temp == null)
            return null;


        List<Listing> feat = new ArrayList<>();
        List<Listing> norm = new ArrayList<>();

        if(temp.getFeaturedListings() != null)
            feat.addAll(temp.getFeaturedListings());
        if(i == categories.size() - 1 && temp.getNormalListings() != null)
            norm.addAll(temp.getNormalListings());

        i++;

        while(i < categories.size()) {
            temp = temp.getChildWithName(categories.get(i));
            if(temp.getFeaturedListings() != null)
                feat.addAll(temp.getFeaturedListings());
            if(i == categories.size() - 1 && temp.getNormalListings() != null)
                norm.addAll(temp.getNormalListings());
            i++;
        }

        Map<String, List<Listing>> map = new HashMap<>();
        map.put("featured", feat);
        map.put("normal", norm);
        return map;
    }
	public boolean categoryExists(String name) {
		return (findCategoryName(name) != null);
	}

	
	public Category findCategoryName(String name) {
		Category node = null;
		
		if(!isEmpty()) {
			node = auxFindCategoryName(root, name.toLowerCase());
		}
		return node;
	}
	
	private Category auxFindCategoryName(Category current, String name) {	
		Category returnnode = null;
		int i = 0;
		if(current.getName().equals(name)) {
			returnnode = current;
		} else if (current.hasChildren()) {
			i = 0;
			while(returnnode == null && i < current.getNumberOfChildren()) {
				returnnode = auxFindCategoryName(current.getChildAt(i), name);
				i++;
			}
		}
		return returnnode;
	}
	
	public Category findListing(Listing findthis) {
		Category node = null;
		
		if(!isEmpty()) {
			node = auxFindListing(root, findthis);
		}
		
		return node;
	}
	
	private Category auxFindListing(Category current, Listing findthis) {
		Category returnnode = null;
		int i = 0;
		
		if(current.hasListing(findthis)) {
			returnnode = current;
		} else if ( current.hasChildren() ) {
			i = 0;
			while(returnnode == null && i < current.getNumberOfChildren()) {
				returnnode = auxFindListing(current.getChildAt(i), findthis);
				i++;
			}
		}
		return returnnode;
	}

	public List<Listing> preOrderTraversal() {
		List<Listing> list = new ArrayList<Listing>();
		
		if(!isEmpty())
			auxPreOrderTraversal(root, list);
		else
			return null;
		
		return list;
	}
	
	private List<Listing> auxPreOrderTraversal(Category current, List<Listing> list) {
		list.addAll(current.getListings());
		for(Category cat: current.getChildren()) {
			auxPreOrderTraversal(cat, list);
		}
		return list;
	}
	
	private String inOrderTraversal() {
		Queue<Category> queue = new ArrayDeque<>();
		queue.add(root);
		int i = 0;
		String str = "";
		
		while(!queue.isEmpty()) {
			Category current = queue.remove();
			if(!current.toString().equals(current.getName()))
					str += current.toString() + "\n";
			if(current.hasChildren()) {
				for(i = 0; i < current.getNumberOfChildren(); i++) {
					queue.add(current.getChildAt(i));
				}
			}
		}
		return str;
	}

	public List<String> traverse()
	{
		if(root == null)
			return null;


		List<String> str = new ArrayList<>();
		return auxTraverse(root, str);
	}

	public List<String> auxTraverse(Category cat, List<String> str) {

		if(!cat.hasChildren())
			return str;

		for (Category child : cat.getChildren()) {
			str.add(child.toString());
			return auxTraverse(child, str);
		}

		return str;
	}

	public List<Searchable> matches(String query) {
		if(!isEmpty()) {
			List<Searchable> matching = new ArrayList<>();
			return auxMatches(root, query, matching);
		}

		return null;
	}

	private List<Searchable> auxMatches(Category cat, String query, List<Searchable> matching) {
		if(cat.matches(query)) {
			matching.add(cat);
		}

		for(Category child : cat.getChildren()) {
			auxMatches(child, query, matching);
		}

		return matching;
	}

	public boolean isEmpty() {
		return root == null;
	}
	
	public String toString() {
		return "" + inOrderTraversal();
	}
	
	
}
