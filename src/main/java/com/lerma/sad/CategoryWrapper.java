package com.lerma.sad;

/**
 * Created by alexanderlerma on 2/29/16.
 */
public class CategoryWrapper {

    private String parent;
    private String name;

    public CategoryWrapper() { }

    public CategoryWrapper(String name) {
        this.name = name;
        this.parent = null;
    }

    public CategoryWrapper(String name, String parent) {
        this.parent = parent;
        this.name = name;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category makeCategory() {
        return new Category(parent, name);
    }

    public String toString() {
        return "Parent: " + parent + "\n"
                + " Name: " + name;
    }

}
