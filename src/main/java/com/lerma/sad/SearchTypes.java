package com.lerma.sad;

public interface SearchTypes {
	public int LASTNAME = 4;
	public int PHONE = 5;
	public int START = 6;
	public int END = 7;
	public int PRICE = 8;
	public int EMAIL = 9;
	public int BUSINESS = 10;
	public int RANDOM = 11;
	public int FB = 12;
	public int TWIT = 13;
	public int LI = 14;
}
