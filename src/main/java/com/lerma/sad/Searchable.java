package com.lerma.sad;

/**
 * Created by alexanderlerma on 2/19/16.
 */
public interface Searchable {

    boolean matches(String str);
}