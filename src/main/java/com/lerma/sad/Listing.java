package com.lerma.sad;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.HashMap;
import java.util.Map;

@XmlRootElement
public class Listing implements ListingTypes, Searchable {
	
	private static int IDCOUNTER = 0;
	private int id, advertiserID, type;
	private double price;
	private Dates start, end;
	private String category, description, website, image;
	
	public Listing() { }
	
	public Listing(int id, int advertiserID, int type,  double price, String category, String description, String website, String image, Dates start, Dates end) {
		this.id = id;
		this.advertiserID = advertiserID;
		this.type = type;
		this.price = price;
		this.category = category;
		this.start = start;
		this.end = end;
		this.description = description;
		this.website = website;
		this.image = image;
	}

	public static boolean validListing(Listing l) {
		return l.category != null && l.description != null && l.website != null && l.image != null && l.start != null && l.end != null;
	}
	public static int getCurrentIDCount() {
		return IDCOUNTER;
	}
	
	public static int getThenIncrementCount() {
		return IDCOUNTER++;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getID() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getAdvertiserID() {
		return advertiserID;
	}
	
	public void setAdvertiserID(int advertiserID) {
		this.advertiserID = advertiserID;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}

	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Dates getStartDate() {
		return start;
	}
	
	public void setStartDate(Dates startDate) {
		this.start = startDate;
	}
	
	public Dates getEndDate() {
		return end;
	}
	
	public void setEndDate(Dates endDate) {
		this.end = endDate;
	}

	public String getImageFile() {
		return image;
	}
	
	public void setImageFile(String imageFile) {
		this.image = imageFile;
	}

	public String getWebsiteLink() {
		return website;
	}

	public void setWebsiteLink(String websiteLink) {
		this.website = websiteLink;
	}

	public String getAdvName() {
		return SAD.INSTANCE.getAdvertiser(advertiserID).getAdvertiser().getName();
	}

	public boolean equals(Listing l) {
		System.out.println("CALLED!");
		return  id == l.id && advertiserID == l.advertiserID && type == l.type
				&& price == l.price && start.equals(l.start) && end.equals(l.end)
				&& description.equals(l.description) && website.equals(l.website)
				&& image.equals(l.image) && category.equals(l.category);
	}

	public int compareToByMostRecent(Listing l) {
		int thistime = start.calculateDaysFromNowAndDate();
		int thattime = l.getStartDate().calculateDaysFromNowAndDate();
		if(thistime < thattime) 
			return -1;
		else if(thistime > thattime) 
			return 1;
		else
			return 0;
	}
	
	public int compareToByMostProfittable(Listing l) {
		double firstProfit = price / start.calculateDurationInDays(end);
		double secondProfit = l.getPrice() / l.getStartDate().calculateDurationInDays(l.getEndDate());
		if(firstProfit < secondProfit)
			return -1;
		else if (firstProfit > secondProfit)
			return 1;
		else
			return 0;
	}

	public boolean matches(String str) {

		str = str.toLowerCase();
		String[] arr = {category, description, website, image};
		for(String s : arr) {
			if(s == null)
				continue;
			if(s.toLowerCase().contains(str))
				return true;
		}
        return false;
	}

	public Map<String, Integer> getIDAsHashMap() {
		Map<String, Integer> map = new HashMap<>(1);
		map.put("id", id);
		return map;
	}
	
	public String toString() {
		return  "ID: " + id + "\n" +
				"Start Date: " + start.toString() + "\n" +
				"End Date: " + end.toString() + "\n" +
				"Description: " + description + "\n" +
				"Website Link: " + website + "\n" +
				"Image: " + image;
	}
	


}
