package com.lerma.sad;

import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.UrlValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validators {


	public static boolean isValidImageFile(String img) {
		char[] a = img.toCharArray();
		img = "";
		int i = 0;
		for (i = 0; i < a.length; i++) {
			if (a[i] == '.') {
				break;
			}
		}
		while (++i < a.length) {
			img += a[i];
		}
		return img.equals("jpg") || img.equals("jpeg");
	}
	
	public static boolean isValidEmail(String email) {
		return EmailValidator.getInstance().isValid(email);

	}

	public static boolean isValidPhoneNumber(String phone) {
		Pattern pattern = Pattern.compile("\\d{3}-\\d{3}-\\d{4}");
		Matcher matcher = pattern.matcher(phone);
		return matcher.matches();
	}

	public static boolean isValidURL(String link) {
		String[] schemes = { "http", "https" };
		UrlValidator urlv = new UrlValidator(schemes);
		return urlv.isValid(link);
	}
}
