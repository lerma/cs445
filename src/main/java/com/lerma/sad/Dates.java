package com.lerma.sad;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Duration;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Dates {

	private String day;
	private String month; 
	private String year;
	
	public Dates() { }
	
	public Dates(String month, String day, String year) {
		this.month = month; 
		this.day = day;
		this.year = year;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}
	
	public String createDateString() {
		return month + "/" + day + "/" + year;
	}
	
	public String createDateString(String month, String day, String year) {
		return month + "/" + day + "/" + year;
	}
	
	public DateTime createDateTimeObject() {
		DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy");
		String adate = createDateString();
		DateTime joda = null;
		try {
			joda = dtf.parseDateTime(adate);
		} catch(IllegalFieldValueException e) {
			System.out.println(e.getMessage());
		}
		return joda;
	}
	
	public DateTime createDateTimeObject(String month, String day, String year) {
		DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy");
		String date = createDateString(month, day, year);
		DateTime joda = null;
		try {
			joda = dtf.parseDateTime(date);
		} catch(IllegalFieldValueException e) {
			System.out.println(e.getMessage());
		}
		return joda;
	}
	
	public int calculateDurationInDays(Dates other) {
		Duration duration = new Duration(createDateTimeObject(), other.createDateTimeObject());
		return (int) Math.abs(duration.getStandardDays());
	}
	
	public int calculateDaysFromNowAndDate() {
		DateTime now = new DateTime();
		return Days.daysBetween(now.toLocalDate(), createDateTimeObject().toLocalDate()).getDays();
	}
	
	public boolean isValidDate(String month, String day, String year) {
		DateTime dt = createDateTimeObject(month, day, year);
		if(dt == null)
			return false;
		if(dt.isBeforeNow())
			return false;
		
		return true;
	}
	
	public boolean hasStarted() {
		return calculateDaysFromNowAndDate() < 0;
	}
	
	public boolean hasDateBeenPassed() {
		return calculateDaysFromNowAndDate() < 0;
	}
	
	public String toString() {
		DateTime joda = createDateTimeObject();
		if(joda == null)
			return "Illegal Date \n";
		else
			return joda.toString("MM/dd/yyyy");
	}
	
	
}
