package com.lerma.sad;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/csr")
public class CustomerServiceResource {

	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/search")
	public Response csrQuery(@QueryParam("q") String query) {
		List<Searchable> results = SAD.INSTANCE.queryResults(query);
		if(results == null)
			return Response.status(Response.Status.BAD_REQUEST).build();

		return Response.ok(results).build();
	}
}
