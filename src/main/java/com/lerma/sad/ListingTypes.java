package com.lerma.sad;

public interface ListingTypes {

	int NORMAL = 1;
	int HOMEPAGE = 2;
	int FEATURED = 3;

}
