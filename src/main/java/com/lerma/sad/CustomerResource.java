package com.lerma.sad;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Path("/")
public class CustomerResource implements ListingTypes {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHomePageListings() {
		List<Listing> list = SAD.INSTANCE.getListingsByType(HOMEPAGE);
		return Response.ok(list).build();
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{cid : .+}")
	public Response getListingsForCategories(@PathParam("cid") String categories) {
		List<String> list = Arrays.asList(categories.split("/"));
		list = list.stream().map(x -> x.toLowerCase()).collect(Collectors.toList()); //lowercase the query
		Map<String, List<Listing>> map = SAD.INSTANCE.getListingsForACategory(list);
		if (map == null)
			return Response.ok("Nothing").build();
		return Response.ok(map).build();
	}
}
