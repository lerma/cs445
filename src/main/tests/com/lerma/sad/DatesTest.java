package com.lerma.sad;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DatesTest {
	
	private Dates d1;
	private Dates d2;
	private Dates d3;
	
	public DatesTest() {
		d1 = new Dates("12","19","2016");
		d2 = new Dates("11", "19", "2016");
		d3 = new Dates("07", "11", "2012");
	}

	@Test
	public void testCalculateDurationInDays() {
		int test = d1.calculateDurationInDays(d2);
		assertEquals(test, 30);
	}
	
	@Test
	public void testCalculateDaysFromNowAndDate() {
		int test = d1.calculateDaysFromNowAndDate();
		DateTime d = new DateTime();
		assertEquals(test, Math.abs(Days.daysBetween(d1.createDateTimeObject().toLocalDate(), d.toLocalDate()).getDays()));
	}
	
	@Test
	public void testHasDateBeenPassed() {
		assertTrue(d3.hasDateBeenPassed());
	}

}
