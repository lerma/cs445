package com.lerma.sad;

import static org.junit.Assert.*;
import org.junit.Test;

import java.util.Arrays;

public class SADTest {
	private Advertiser adv;
	private Listing listing;
	
	public SADTest() {

		adv = new Advertiser(13, "Alexander", "a@gmail.com", "361-585-9868", new Address("122 E 32nd ST.", "Chicago", "IL", "60616"),"www.facebook.com", "www.twitter.com", "www.linkedin.com");
		listing = new Listing(1, 13, ListingTypes.FEATURED, 300.00, "baseball", "NICEEEE", "www.cool.com", "cool.jpeg",new Dates("03", "16", "2016"), new Dates("05","16","2016"));
	}

	@Test
	public void testAddAdvertiser() {
		SAD.INSTANCE.addAdvertiser(adv);
		assertEquals(adv, SAD.INSTANCE.getAdvertiser(13).getAdvertiser());
	}
	
	@Test
	public void testAddListing() {
		SAD.INSTANCE.addAdvertiser(adv);
		SAD.INSTANCE.addListing(listing);
		assertEquals(SAD.INSTANCE.getAdvertiser(13).getListingsByType(ListingTypes.FEATURED).get(0), listing);
	}
	
	@Test
	public void testAddMainCategory() {
		SAD.INSTANCE.getCategories().addCategory("root", "sports");
		assertTrue(SAD.INSTANCE.getCategories().categoryExists("sports"));	
	}
	
	@Test
	public void testAddSubCategory() {
		SAD.INSTANCE.getCategories().addCategory("root", "sports");
		SAD.INSTANCE.getCategories().addCategory("sports", "baseball");
		assertTrue(SAD.INSTANCE.getCategories().categoryExists("baseball"));
	}

	@Test
	public void testGetListingsForACategory() {
		SAD.INSTANCE.getCategories().addCategory("root", "sports");
		SAD.INSTANCE.getCategories().addCategory("sports", "baseball");
		SAD.INSTANCE.addAdvertiser(adv);
		SAD.INSTANCE.addListing(listing);
		assertEquals(SAD.INSTANCE.getListingsForACategory(Arrays.asList("sports", "baseball")).get("featured").get(0), listing);
	}

}
