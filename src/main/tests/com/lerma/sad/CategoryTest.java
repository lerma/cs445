package com.lerma.sad;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 * Created by alexanderlerma on 2/29/16.
 */
public class CategoryTest {


    private Category cat;

    public CategoryTest() {
        cat = new Category("sports");
        cat.addChild(new Category("sports", "baseball"));
    }


    @Test
    public void testRemoveChildByName() {
        cat.removeChildByName("baseball");
        assertFalse(cat.hasChildren());
    }

    @Test
    public void testAddListing()  {
        Listing l = new Listing(12, 1, ListingTypes.NORMAL, 300.00, "sports",  "cool", "www.cool.com", "cool.jpeg",new Dates("03", "16", "2016"), new Dates("05","16", "2016"));
        cat.addListing(l);
        assertTrue(cat.getListings().contains(l));
    }

    @Test
    public void testGetFeaturedListings() {
        Listing l = new Listing(12, 1, ListingTypes.FEATURED, 300.00, "sports",  "cool", "www.cool.com", "cool.jpeg",new Dates("03", "16", "2016"), new Dates("05","16", "2016"));
        cat.addListing(l);
        assertTrue(cat.getFeaturedListings().contains(l));
    }

    @Test
    public void testGetNormalListings() {
        Listing l = new Listing(12, 1, ListingTypes.NORMAL, 300.00, "sports",  "cool", "www.cool.com", "cool.jpeg",new Dates("03", "16", "2016"), new Dates("05","16", "2016"));
        cat.addListing(l);
        assertTrue(cat.getNormalListings().contains(l));
    }
}