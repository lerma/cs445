package com.lerma.sad;

import static org.junit.Assert.*;
import org.junit.Test;

public class ListingTest implements ListingTypes {
	
	private Listing newer;
	private Listing older;
	private Listing invalid;
	
	public ListingTest() {
		newer = new Listing(12, 1, NORMAL, 300.00, "baseball",  "baseball products for you", "www.baseball.com", "baseball.jpeg",new Dates("03", "18", "2016"), new Dates("05","16", "2016"));
		older = new Listing(13, 1, FEATURED, 55.00, "soccer",  "soccer products for you", "www.soccer.com", "soccer.jpeg",new Dates("03", "18", "2016"), new Dates("03","20", "2016"));
		invalid = new Listing(13, 1, FEATURED, 55.00, "soccer",  null, "www.soccer.com", "soccer.jpeg",new Dates("03", "18", "2016"), new Dates("03","20", "2016"));
	}

	@Test
	public void testValidListing() {
		assertTrue(Listing.validListing(newer));
		assertFalse(Listing.validListing(invalid));
	}

	@Test
	public void testMoreProfitable() {
		assertTrue(newer.compareToByMostProfittable(older) == -1);
	}

}
