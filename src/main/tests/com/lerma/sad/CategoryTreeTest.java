package com.lerma.sad;


import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CategoryTreeTest {
	
	
	private CategoryTree tree;

	public CategoryTreeTest() {
		tree = new CategoryTree("root");
	}

	@Test
	public void testAddCategory() {
		tree.addCategory("root", "sports");
		assertTrue(tree.categoryExists("sports"));
	}
	
	@Test
	public void testGetNumberOfNodes() {
        CategoryTree atree = new CategoryTree("root");
        atree.addCategory("root", "sports");
        assertEquals(2, atree.getNumberOfNodes());
	}
	
	@Test
	public void testInsertListingToCategory() {
		tree.addCategory("root", "sports");
		Listing l = new Listing(12, 1, ListingTypes.NORMAL, 300.00, "sports",  "cool", "www.cool.com", "cool.jpeg",new Dates("03", "16", "2016"), new Dates("05","16", "2016"));
		tree.insertListingToCategory(l);
		Category cat = tree.findListing(l);
		assertTrue(cat.hasListing(l));
	}

	@Test
	public void testGetListingsForCategory() {
		tree.addCategory("root", "sports");
		tree.addCategory("sports", "baseball");
		Listing l = new Listing(12, 1, ListingTypes.NORMAL, 300.00, "baseball",  "cool", "www.cool.com", "cool.jpeg",new Dates("03", "16", "2016"), new Dates("05","16", "2016"));
		tree.insertListingToCategory(l);
		List<String> cats = new ArrayList<>();
		cats.add("sports");
		cats.add("baseball");
		assertEquals(tree.getListingsForACategory(cats).get("normal").get(0), l);

	}

	@Test
	public void testDeleteCategory() {
        CategoryTree atree = new CategoryTree("root");
		atree.addCategory("root", "sports");
		atree.addCategory("sports", "baseball");
		atree.addCategory("baseball", "bats");
		assertTrue(atree.deleteCategory("baseball"));
	}
	

}
