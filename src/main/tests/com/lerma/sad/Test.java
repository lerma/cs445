package com.lerma.sad;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class Test {

	public static void main(String[] args) {
	    SAD.INSTANCE.getCategories().addCategory("root", "sports");
	    SAD.INSTANCE.getCategories().addCategory("root", "food");
	    SAD.INSTANCE.getCategories().addCategory("sports", "baseball");
	    SAD.INSTANCE.getCategories().addCategory("food", "chinese");
        SAD.INSTANCE.getCategories().addCategory("food", "italian");
        System.out.println(SAD.INSTANCE.getCategories().categoryExists("food"));
		SAD.INSTANCE.addAdvertiser(new Advertiser(1, "Alex", "A@gmail.com", "361-585-9868", new Address("122 E 32nd", "Chicago", "IL", "60616")));
		Listing l = new Listing(12, 1, ListingTypes.HOMEPAGE, 300.00, "baseball",  "cool", "www.cool.com", "cool.jpeg",new Dates("03", "16", "2016"), new Dates("05","16", "2016"));
		SAD.INSTANCE.addListing(l);
        System.out.println(SAD.INSTANCE.getCategories().findListing(l));
        System.out.println(SAD.INSTANCE.getListingByID(12 ));


		Map<String, List<Listing>> list = SAD.INSTANCE.getListingsForACategory( Arrays.asList("sports", "baseball"));
        for(String cat : list.keySet()) {
            System.out.println(cat);
            System.out.println(list.get(cat));
        }
		
	}

}
